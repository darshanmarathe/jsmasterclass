class Flat {
    constructor(private NoOfRooms: number,
        private flatNo: string) {
        
    }

    public GetDetails() {
        return this.NoOfRooms.toString() + " " + this.flatNo;
    }
}

class Floor{
    constructor(private flats:Flat[], private  floorNumber:string) {
        
    }

    public GetDetails() {
        return `${this.flats.length} of flats and number ${this.floorNumber}`;
    }

}

class Building{
    
    
    constructor(private floors: Floor[], private Name: string) {
        
    }

    public GetDetails() {
        return `building name is ${this.Name} having ${this.floors.length} of floors`
    }
}