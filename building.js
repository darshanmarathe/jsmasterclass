var Flat = /** @class */ (function () {
    function Flat(NoOfRooms, flatNo) {
        this.NoOfRooms = NoOfRooms;
        this.flatNo = flatNo;
    }
    Flat.prototype.GetDetails = function () {
        return this.NoOfRooms.toString() + " " + this.flatNo;
    };
    return Flat;
}());
var Floor = /** @class */ (function () {
    function Floor(flats, floorNumber) {
        this.flats = flats;
        this.floorNumber = floorNumber;
    }
    Floor.prototype.GetDetails = function () {
        return this.flats.length + " flats and number " + this.floorNumber;
    };
    return Floor;
}());
var Building = /** @class */ (function () {
    function Building(floors, Name) {
        this.floors = floors;
        this.Name = Name;
    }
    Building.prototype.GetDetails = function () {
        return "building name is " + this.Name + " having " + this.floors.length + " of floors";
    };
    return Building;
}());

// typescript code 

/* class Flat {
    constructor(private NoOfRoo
        ms: number,
        private flatNo: string) {
        
    }

    public GetDetails() {
        return this.NoOfRooms.toString() + " " + this.flatNo;
    }
}

class Floor{
    constructor(private flats:Flat[], private  floorNumber:string) {
        
    }

    public GetDetails() {
        return `${this.flats.length} of flats and number ${this.floorNumber}`;
    }

}

class Building{
    constructor(private floors: Floor[], private Name: string) {
        
    }

    public GetDetails() {
        return `building name is ${this.Name} having ${this.floors.length} of floors`
    }
}
*/